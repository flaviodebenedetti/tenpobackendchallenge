package com.tenpo.challenge.backend.application.events.read;

import com.tenpo.challenge.backend.domain.events.Event;
import com.tenpo.challenge.backend.domain.events.TraceabilityHistoryEvent;
import com.tenpo.challenge.backend.domain.traceabilityHistory.TraceabilityHistory;
import com.tenpo.challenge.backend.infrastructure.port.output.TraceabilityHistoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class ConsumerEventsListener {

    private final TraceabilityHistoryRepository repository;

    @KafkaListener(
            topics = "${spring.kafka.topic.name}",
            groupId = "${spring.kafka.consumer.group-id}",
            autoStartup = "true",
            containerFactory = "kafkaListenerContainerFactory"
    )
    public void listener(Event event){

        log.info("Received message");

        TraceabilityHistoryEvent traceabilityHistoryEvent = (TraceabilityHistoryEvent) event;

        TraceabilityHistory traceabilityHistory = (TraceabilityHistory) event.getData();

        repository.save(TraceabilityHistory.builder()
                .id(traceabilityHistoryEvent.getId())
                .date(traceabilityHistoryEvent.getDate())
                .endpointCalled(traceabilityHistory.getEndpointCalled())
                .response(traceabilityHistory.getResponse())
                .build());

        log.info("Save message listener");
    }
}
