package com.tenpo.challenge.backend.domain.events;

import com.tenpo.challenge.backend.domain.traceabilityHistory.TraceabilityHistory;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class TraceabilityHistoryEvent extends Event<TraceabilityHistory>{
}
