package com.tenpo.challenge.backend.domain.calculation;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Calculation {
    private float sum;
}
