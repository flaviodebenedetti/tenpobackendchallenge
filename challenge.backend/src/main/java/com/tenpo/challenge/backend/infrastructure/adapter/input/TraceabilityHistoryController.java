package com.tenpo.challenge.backend.infrastructure.adapter.input;

import com.tenpo.challenge.backend.domain.traceabilityHistory.TraceabilityHistory;
import com.tenpo.challenge.backend.domain.traceabilityHistory.model.TraceabilityHistoryResponse;
import com.tenpo.challenge.backend.infrastructure.port.input.TraceabilityHistoryUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/challenge/traceabilityhistory")
@RequiredArgsConstructor
public class TraceabilityHistoryController{

    private final TraceabilityHistoryUseCase traceabilityHistoryUseCase;

    @GetMapping
    public ResponseEntity<Optional> getAll(
            @PageableDefault(page = 0, size = 20)
            @SortDefault.SortDefaults({
                    @SortDefault(sort = "date", direction = Sort.Direction.DESC),
                    @SortDefault(sort = "id", direction = Sort.Direction.ASC)
            })
            Pageable pageable) {
        return ResponseEntity.ok(traceabilityHistoryUseCase.getAll(pageable));
    }

}
