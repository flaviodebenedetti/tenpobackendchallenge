package com.tenpo.challenge.backend.application;

import com.tenpo.challenge.backend.domain.events.EventType;
import com.tenpo.challenge.backend.domain.traceabilityHistory.TraceabilityHistory;
import com.tenpo.challenge.backend.infrastructure.exceptions.ServiceRateException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

@RequiredArgsConstructor
@Component
@Slf4j
public class RateService {

    private final TraceabilityHistoryService traceabilityHistoryService;

    private Float rate;

    @Scheduled(cron = "${spring.data.mock.service.rate.time}")
    public void updateRate(){
        Random random = new Random();
        this.rate = Float.valueOf(random.nextInt(1, 30));
        publish();
    }

    public void publish(){
        log.info("Publish get rate");
        traceabilityHistoryService.save(TraceabilityHistory.builder()
                .id(UUID.randomUUID().toString())
                .date(new Date())
                .endpointCalled("https://servicerate/rate")
                .response(HttpStatus.OK.toString())
                .build(), EventType.GET);
    }

    public Float getRate(){
        if(this.rate == null){
            updateRate();
        }
        return this.rate;
    }
}
