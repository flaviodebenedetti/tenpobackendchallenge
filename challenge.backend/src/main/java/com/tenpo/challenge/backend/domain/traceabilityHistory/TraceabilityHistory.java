package com.tenpo.challenge.backend.domain.traceabilityHistory;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

import java.util.Date;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TraceabilityHistory {
    @Id
    private String id;
    private String endpointCalled;
    private String response;
    private Date date;
}
