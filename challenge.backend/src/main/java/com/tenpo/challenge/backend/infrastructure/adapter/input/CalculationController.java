package com.tenpo.challenge.backend.infrastructure.adapter.input;

import com.tenpo.challenge.backend.domain.calculation.Calculation;
import com.tenpo.challenge.backend.domain.calculation.CalculationRequestBody;
import com.tenpo.challenge.backend.infrastructure.exceptions.ServiceRateException;
import com.tenpo.challenge.backend.infrastructure.port.input.CalculationUseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(value = "/challenge/calculation")
public class CalculationController {

    private final CalculationUseCase calculationUseCase;

    @PostMapping
    public ResponseEntity<Optional<Calculation>> createCalculation(@RequestBody CalculationRequestBody calculationRequestBody){
        return ResponseEntity.ok(calculationUseCase.createCalculation(calculationRequestBody));
    }
}
