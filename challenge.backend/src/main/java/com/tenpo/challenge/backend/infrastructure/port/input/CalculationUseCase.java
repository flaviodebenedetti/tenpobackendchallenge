package com.tenpo.challenge.backend.infrastructure.port.input;

import com.tenpo.challenge.backend.domain.calculation.Calculation;
import com.tenpo.challenge.backend.domain.calculation.CalculationRequestBody;

import java.util.Optional;

public interface CalculationUseCase {
    Optional<Calculation> createCalculation(CalculationRequestBody calculationRequestBody);
}
