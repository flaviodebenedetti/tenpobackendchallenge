package com.tenpo.challenge.backend.domain.calculation;

import com.fasterxml.jackson.annotation.JsonProperty;

public record CalculationRequestBody(@JsonProperty("primervalor")int value1, @JsonProperty("segundovalor")int value2) {
}
