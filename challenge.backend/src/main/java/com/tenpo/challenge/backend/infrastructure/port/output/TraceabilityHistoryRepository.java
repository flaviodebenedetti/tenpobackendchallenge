package com.tenpo.challenge.backend.infrastructure.port.output;

import com.tenpo.challenge.backend.domain.traceabilityHistory.TraceabilityHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TraceabilityHistoryRepository extends JpaRepository<TraceabilityHistory, String> {
}
