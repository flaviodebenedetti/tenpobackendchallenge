package com.tenpo.challenge.backend.application;

import com.tenpo.challenge.backend.application.events.write.ProducerEventsService;
import com.tenpo.challenge.backend.domain.events.EventType;
import com.tenpo.challenge.backend.domain.traceabilityHistory.TraceabilityHistory;
import com.tenpo.challenge.backend.domain.traceabilityHistory.model.TraceabilityHistoryResponse;
import com.tenpo.challenge.backend.infrastructure.port.input.TraceabilityHistoryUseCase;
import com.tenpo.challenge.backend.infrastructure.port.output.TraceabilityHistoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.*;

@RequiredArgsConstructor
@Component
@Slf4j
public class TraceabilityHistoryService implements TraceabilityHistoryUseCase {

    private final TraceabilityHistoryRepository traceabilityHistoryRepository;

    private final ProducerEventsService producerEventsService;

    @Override
    public void save(TraceabilityHistory traceabilityHistory, EventType eventType) {
        producerEventsService.publish(traceabilityHistory, eventType);
    }

    @Override
    public Optional getAll(Pageable pageable) {

        Page<TraceabilityHistory> traceabilityHistoryPage = traceabilityHistoryRepository.findAll(pageable);

        if(traceabilityHistoryPage.isEmpty()){
            return Optional.empty();
        }

        TraceabilityHistoryResponse traceabilityHistoryResponse = new TraceabilityHistoryResponse();
        traceabilityHistoryResponse.setContent(traceabilityHistoryPage.getContent());
        traceabilityHistoryResponse.setPageNo(traceabilityHistoryPage.getNumber());
        traceabilityHistoryResponse.setPageSize(traceabilityHistoryPage.getSize());
        traceabilityHistoryResponse.setTotalElements(traceabilityHistoryPage.getTotalElements());
        traceabilityHistoryResponse.setTotalPages(traceabilityHistoryPage.getTotalPages());
        traceabilityHistoryResponse.setLast(traceabilityHistoryPage.isLast());

        return Optional.of(traceabilityHistoryResponse);
    }
}
