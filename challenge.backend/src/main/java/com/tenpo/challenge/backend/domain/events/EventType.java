package com.tenpo.challenge.backend.domain.events;

public enum EventType {
    CREATE, GET
}
