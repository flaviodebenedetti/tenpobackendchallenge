package com.tenpo.challenge.backend.infrastructure.port.input;

import com.tenpo.challenge.backend.domain.events.EventType;
import com.tenpo.challenge.backend.domain.events.TraceabilityHistoryEvent;
import com.tenpo.challenge.backend.domain.traceabilityHistory.TraceabilityHistory;
import com.tenpo.challenge.backend.domain.traceabilityHistory.model.TraceabilityHistoryResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public interface TraceabilityHistoryUseCase {
    void save(TraceabilityHistory traceabilityHistory, EventType eventType);
    Optional getAll(Pageable pageable);
}
