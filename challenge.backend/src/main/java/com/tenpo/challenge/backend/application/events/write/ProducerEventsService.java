package com.tenpo.challenge.backend.application.events.write;

import com.tenpo.challenge.backend.domain.events.Event;
import com.tenpo.challenge.backend.domain.events.EventType;
import com.tenpo.challenge.backend.domain.events.TraceabilityHistoryEvent;
import com.tenpo.challenge.backend.domain.traceabilityHistory.TraceabilityHistory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
@Slf4j
public class ProducerEventsService {

    private final KafkaTemplate<String, Event<?>> producer;

    @Value("${spring.kafka.topic.name}")
    private String topicTraceabilityHistory;

    public void publish(TraceabilityHistory traceabilityHistory, EventType eventType) {

        TraceabilityHistoryEvent event = new TraceabilityHistoryEvent();
        event.setId(traceabilityHistory.getId());
        event.setDate(new Date());
        event.setType(eventType);
        event.setData(traceabilityHistory);

        log.info("######################");
        log.info("Send message");
        log.info("Event: " + event);
        log.info("######################");

        this.producer.send(topicTraceabilityHistory, event);
    }

}
