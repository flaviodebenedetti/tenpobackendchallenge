package com.tenpo.challenge.backend.infrastructure.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.server.ResponseStatusException;

public class ServiceRateException extends ResponseStatusException {

    public ServiceRateException(HttpStatus httpStatus, String reason) {
        super(httpStatus, reason);
    }
}
