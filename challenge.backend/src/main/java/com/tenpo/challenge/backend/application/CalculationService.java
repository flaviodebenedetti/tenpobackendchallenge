package com.tenpo.challenge.backend.application;

import com.tenpo.challenge.backend.domain.calculation.Calculation;
import com.tenpo.challenge.backend.domain.calculation.CalculationRequestBody;
import com.tenpo.challenge.backend.domain.events.Event;
import com.tenpo.challenge.backend.domain.events.EventType;
import com.tenpo.challenge.backend.domain.events.TraceabilityHistoryEvent;
import com.tenpo.challenge.backend.domain.traceabilityHistory.TraceabilityHistory;
import com.tenpo.challenge.backend.infrastructure.port.input.CalculationUseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Component
@Slf4j
public class CalculationService implements CalculationUseCase {

    private final TraceabilityHistoryService traceabilityHistoryService;
    private final RateService rateService;

    public Optional<Calculation> createCalculation(CalculationRequestBody calculationRequestBody) {

        float rateMock = rateService.getRate();
        float sum = getSum(calculationRequestBody);
        float result = applyRate(sum, rateMock);

        publishTraceability();

        return Optional.of(Calculation.builder().sum(result).build());
    }

    private void publishTraceability() {

        TraceabilityHistory traceabilityHistory = TraceabilityHistory.builder()
                .id(UUID.randomUUID().toString())
                .endpointCalled("localhost:8080/calculation")
                .response(HttpStatus.OK.toString())
                .date(new Date())
                .build();

        traceabilityHistoryService.save(traceabilityHistory, EventType.CREATE);
    }

    private float applyRate(float sum, float rate) {
        return sum + sum * rate / 100;
    }

    private float getSum(CalculationRequestBody calculationRequestBody) {
        return calculationRequestBody.value1() + calculationRequestBody.value2();
    }
}
